# preview script steps before running

# You can run the script with the DRY_RUN=1 option to learn what steps the script will execute during installation:

 curl -fsSL https://get.docker.com -o get-docker.sh
 DRY_RUN=1 sh ./get-docker.sh
