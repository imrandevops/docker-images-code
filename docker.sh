    #! /bin/bash
    
    # Update the installed packages and package cache 
    apt-get update -y
	
    # Set up the repository
    # Update the apt package index and install packages to allow apt to use a repository over HTTPS:

    apt-get apt-get update -y
    apt-get apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y
	
	# Add Docker’s official GPG key:
     curl -fsSL https://download.docker.com/linux/ubuntu/gpg |  gpg --dearmor -o /usr/share/keyrings/      docker-archive-keyring.gpg
	
	#Install Docker Engine
    # Update the apt package index, and install the latest version of Docker Engine and containerd, or go to the next step to install a specific version:

    apt-get update
    apt-get install docker-ce docker-ce-cli containerd.io
	
	# Install the docker service
	apt-get install docker.io
	
    # Start the Docker service.
    service docker start
   
	# Configure Docker to start on boot
	apt-get systemctl enable docker
	
